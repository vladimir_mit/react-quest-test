import React from 'react';

const Slider = () =>
    (<section class="slider">
        <button class="slider__button-checked slider__button-checked_left">&#9668;</button>  
        <div class="slider__item" id="slideOne">
            <img src="./photo_slide_valak.jpg" alt="sad" class="slaider__image" />
            <div class="slider__item-wrapper">
                <h2 class="slider__item-titel">Валак</h2>
                <div class="slider__button-wrapper">
                </div>
            </div>
        </div>

        <div class="slider__item" id="slideTwo">
            <img src="./photo_slide_mumiya.jpg" alt="sad" class="slaider__image" />
            <div class="slider__item-wrapper">
                <h2 class="slider__item-titel">Мумия</h2>
                <div class="slider__button-wrapper">
                    
                </div>
            </div>
        </div>
        <button class="slider__button-checked slider__button-checked_right">&#9658;</button>
    </section>
    );



export default Slider