import React, { useEffect, setState } from 'react';
import logo from './logo.svg';
import './App.css';
import Main from './Main/Main.js'
import Quest from './Quest/Quest'
import NavigationMenu from './NavigationMenu/NavigationMenu'
import { Switch, Route } from 'react-router-dom';
import preloader from './resources/images/mainPreloader.png' //'./resources/images/loader.gif'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const reducer = (state, action) => {
  switch (action.type) {
    case 'updateBusy':
      const newState = Object.assign({}, state)
      newState.quests.find(q => q.name == action.payload.qName).busy.push(action.payload.value)
      return newState
    case 'updateState':
      return Object.assign({}, action.payload.value)
    case 'loadQuest':
      const stateWithQuestData = Object.assign({}, state);
      let questIndex = stateWithQuestData.quests.findIndex(q => q.id == action.payload.value.id)//
      stateWithQuestData.quests[questIndex] = action.payload.value;
      return stateWithQuestData
    case 'selectQuestDateTime':
      const stateWithSelectedDateTime = Object.assign({}, state);
      questIndex = stateWithSelectedDateTime.quests.findIndex(q => q.id == action.payload.questId)
      stateWithSelectedDateTime.quests[questIndex].SelectedDateTime = action.payload.dateTime;
      return stateWithSelectedDateTime
    case 'userInfoUpdate':
      const stateWithUserInfo = Object.assign({}, state);
      if (stateWithUserInfo.UserInfo === undefined) stateWithUserInfo.UserInfo = {}
      stateWithUserInfo.UserInfo[action.payload.field] = action.payload.value;
      return stateWithUserInfo
    default:
      return state
  }
}

const NotFound = () => (<div>NotFound</div>)

const App = () => {
  const [state, dispatch] = React.useReducer(reducer);

  useEffect(() => {
    fetch(process.env.REACT_APP_COMPANY_URL)
      .then(resp => resp.json())
      .then(data => dispatch({ type: "updateState", payload: { value: data } }))
      .catch(err => NotificationManager.error('Error message', err.message, 5000, () => {
        alert(err);
      }))
  }, [])

  if (state === undefined) {
    return (
      <div className="App">
          <NotificationContainer/>
          <img className = 'blink_img centered' src={preloader}></img>
      </div>
    )
  }

  return (
    <div className="App">
      <NotificationContainer />
      <NavigationMenu quests={state.quests} />
      <Switch>
        <Route exact path='/' component={() => <Main name={state.companyName} contacts={state.contacts} description={state.companyDescription} />} />
        {state.quests
          .map(x => <Route key={x.id}
            path={`/quest/${x.id}`}
            component={
              () => <Quest key={x.name} quest={x} dispatch={dispatch}
                onClick={(dateTime) => {
                  dispatch({ type: "selectQuestDateTime", payload: { questId: x.id, dateTime: dateTime} });
                }}
                onInput={(field, value) => { 
                  dispatch({ type: "userInfoUpdate", payload: { field: field, value: value} });
                }}
                UserInfo={state.UserInfo}
              />}
          />)}
        <Route component={NotFound} />
      </Switch>
    </div>
  );
}

export default App;
