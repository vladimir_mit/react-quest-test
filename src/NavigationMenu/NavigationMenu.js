﻿import React from 'react'
import { Link } from 'react-router-dom'
import Quest from '../Quest/Quest'
import logo from '../resources/images/main.png'
import './NavigationMenu.css'

const NavigationMenu = (props) => {
    return (
        <div className='main-menu'>
            <img className='logo' src={logo}></img>

            <button className='button__menu-toggle'> Меню </button>
            <div className='menu-wrapper'>
                <div className='menu-element'>
                    <Link to='/'>О нас</Link>
                </div>
                <div>
                    <div className='dropdown menu-element'>
                        <span className="dropbtn">Квесты &#9660;</span>
                        <div className='dropdown-content menu-element'>
                            {props.quests.map(x => <div key={x.id}><Link to={`/quest/${x.id}`}>{x.name}</Link></div>)}
                        </div>
                    </div>
                </div>
                <a href="#Location" className='menu-element'>Контакты</a>
                <a href="#Location" className='menu-element'>Запись</a>
                <a href="#Location" className='menu-element'>Место</a>
                <a href="#Location" className='menu-element'>Галерея</a>
            </div>
        </div>
    )
}

export default NavigationMenu


