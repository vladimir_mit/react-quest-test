import React, { useEffect } from 'react'
import TimeTable from './TimeTable';
import './Quest.css'
import { NotificationManager } from 'react-notifications';
import preloader from '../resources/images/mainPreloader.png' //'../resources/images/loader.gif'

const Quest = (props) => {
    if (props.quest.description === undefined)
        fetch(`${process.env.REACT_APP_QUEST_URL}/${props.quest.id}`)
            .catch(err => NotificationManager.error('Error message', err.message, 5000, () => {
                alert(err);
            }))
            .then(resp => resp.json())
            .then(data => props.dispatch({ type: "loadQuest", payload: { value: data } }));
            let startDate = new Date();
            startDate.setDate(startDate.getDate() - 1);
    return (
        <div>
            <div>
                Quest {props.quest.name}
            </div>
           { 
            props.quest.description === undefined ?
            (<img className='blink_img centered' src={preloader}></img>) : 
            (<div>
                Description {props.quest.description}     
            <TimeTable busy={props.quest.busy} 
            step={props.quest.questTime + props.quest.prepeareTime} 
            openHours={props.quest.openHours} 
            startDate={startDate} 
            daysCount={14} 
            questId={props.quest.id}
            selectedDateTime={props.quest.SelectedDateTime}
            UserInfo={props.UserInfo}
            onClick={props.onClick}
            onInput={props.onInput} />
            </div>)
            }
            <div id='Location'>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1094.3726164093341!2d37.142525276099995!3d56.73016676659694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b443a5d8b300b7%3A0xea85fca9dd7fb8c1!2z0L_RgC4g0JHQvtCz0L7Qu9GO0LHQvtCy0LAsIDQ00LAsINCU0YPQsdC90LAsINCc0L7RgdC60L7QstGB0LrQsNGPINC-0LHQuy4sIDE0MTk4MQ!5e0!3m2!1sru!2sru!4v1522950710800" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>)
}

export default Quest