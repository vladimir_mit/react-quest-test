import React from 'react'

const TimeTable = (props) => {
    return (
        <div className='TimeTable'>
            <ModalDialog selectedDateTime={props.selectedDateTime} 
            onExitClick={props.onClick} 
            onInput={props.onInput} 
            name={props.UserInfo === undefined ? "" : props.UserInfo.name}
            number={props.UserInfo === undefined ? "" : props.UserInfo.number}
             />
            {Array.from(Array(props.daysCount).keys(), x => {
                const copy = new Date(Number(props.startDate))
                copy.setDate(props.startDate.getDate() + x);
                let hours = Array();
                if (props.openHours[copy.getDay()] === undefined)
                    hours = props.openHours['default'].toString().split('-');
                else
                    hours = props.openHours[copy.getDay()].toString().split('-');

                return (
                    <Row
                        key={x}
                        date={copy}
                        startHour={hours[0]}
                        endHour={hours[1]}
                        step={props.step}
                        busy={props.busy}
                        questId={props.questId}
                        onClick={props.onClick}
                    />
                );
            })}
        </div>
    )
}

const Row = (props) => {
    let startHour = props.startHour.split(':').map(x => parseInt(x));
    let endHour = props.endHour.split(':').map(x => parseInt(x));

    let questLocalTime = new Date();
    let rowRange = range(startHour[0] * 60 + startHour[1], endHour[0] * 60 + endHour[1] + (endHour[0] < startHour[0] ? 24 * 60 : 0), props.step);

    let filtredRowRange = rowRange.map(x => {
        var copy = new Date(Number(props.date))
        if (x / 60 / 24 >= 1)
            copy.setDate(copy.getDate() + 1);
        return {
            'date': copy.getDate(),
            'month': copy.getMonth() + 1,
            'year': copy.getFullYear(),
            'dateTimeString': `${copy.getFullYear()}-${copy.getMonth() + 1}-${copy.getDate()} ${('0' + Math.ceil(x / 60 % 24)).slice(-2)}:${('0' + x % 60).slice(-2)}`,
            'minutes': x
        }
    }).filter(date => new Date(new Date(date.dateTimeString)) > questLocalTime);

    if (filtredRowRange.length == 0)
        return null;

    const dateOptions = { month: 'long', day: 'numeric', weekday: 'long' };

    return (
        <div>
            <div className='row-date'>
                <div className='date'> {props.date.toLocaleDateString(undefined, dateOptions)} </div>
                <div className='row'>
                    {filtredRowRange.map(x => {
                        return <TimeTableItem
                            key={`${x.year}-${x.month}-${x.date}-${x.minutes}`}
                            value={x.dateTimeString}
                            name={`${('0' + Math.ceil(x.minutes / 60 % 24)).slice(-2)}:${('0' + x.minutes % 60).slice(-2)}`}
                            questId={props.questId}
                            onClick={props.onClick}
                            isBusy={props.busy.includes(x.dateTimeString)} />
                    })}
                </div>
            </div>
        </div>
    )
}
const TimeTableItem = (props) => {
    return (
        <a className={`TimeTableItem${props.isBusy ? " not-active" : ""}`} onClick={() => props.onClick(props.value)}>
            {props.name}
        </a>
    )
}

function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    }

    if (typeof step == 'undefined') {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }

    return result;
};

const ModalDialog = (props) => {
    if (props.selectedDateTime === undefined)
        return null;

    return (
        <div className='modal fade show'>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1>Запись на {props.selectedDateTime}</h1>
                        <button onClick={() => props.onExitClick(undefined)} type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div className="modal-body">
                        <form className="form" id="form">
                            <div className="form-group">
                                <input name="name" id="name" onBlur={() => props.onInput("name", document.getElementById("name").value)} defaultValue={ props.name } type="text" placeholder="Ваше имя" required="" class="form-control" />
                            </div>
                            <div class="form-group">
                                <input name="number" id="number" onBlur={() => props.onInput("number", document.getElementById("number").value)} defaultValue={ props.number } required="" type="tel" class="form-control" placeholder="Телефон" />
                            </div>
                            <input type="submit" class="btn btn-primary" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TimeTable