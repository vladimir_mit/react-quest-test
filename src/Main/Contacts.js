import React from 'react'

const Contacts = (props) => {
    return (
    <ul className='contacts'>
        {props.contacts.map(contact => (<ContactItem key={contact.name} name={contact.name} value={contact.value} />))}
    </ul>)
}

const ContactItem = (props) => {
    return (<li className='contacts__item'>{props.name} {props.value}</li>)
}

export default Contacts