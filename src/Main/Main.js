import React from 'react';
import Contacts from './Contacts'
import './Main.css'
import Slider from '../Slider/slider';

const Main = (props) => {
    return (
        <div>
            <h2 className='main__title'>Main {props.name}</h2>
            <Slider/>
            <p className='description'>{props.description}</p>
            <Contacts contacts={props.contacts} />
            <div id='Location'>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1094.3726164093341!2d37.142525276099995!3d56.73016676659694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b443a5d8b300b7%3A0xea85fca9dd7fb8c1!2z0L_RgC4g0JHQvtCz0L7Qu9GO0LHQvtCy0LAsIDQ00LAsINCU0YPQsdC90LAsINCc0L7RgdC60L7QstGB0LrQsNGPINC-0LHQuy4sIDE0MTk4MQ!5e0!3m2!1sru!2sru!4v1522950710800" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>
    )
}

export default Main